var express = require("express");
var app = express();

var server = require("http").Server(app);
var io = require("socket.io")(server, {path: "/testpath/socket.io"});
var _ = require("lodash");


var router = express.Router();

function log(logString) {
    var currentDate = new Date();
    var dateTimeString = currentDate.getHours() + ":"
        + currentDate.getMinutes() + ":"
        + currentDate.getSeconds() + "."
        + currentDate.getMilliseconds();

    console.log(dateTimeString + ": " + logString);
}
var mmRpc = require("./public/js/mmRpc");
var mmRpcServer = new mmRpc.MmRpcServer(io, _, log);

router.get('/', function (request, response) {
    response.sendFile(__dirname + '/index.html');
});

mmRpcServer.on("foo", function (content, sendResponse) {
    sendResponse(content + "_xxx");
});

function scream() {
//    log("screaming...");
    mmRpcServer.notifyClient(
        "client-identifier",
        "server_says",
        "content_for_you",
        {
            maxRetries: 3,
            problem: function () {
                log("problem telling client something.");
            },
            error: function () {
                log("error telling client something.");
            },
            success: function () {
//                log("successfully sent something to a client.");
            },
            complete: function () {
//                log("completed.");
                setTimeout(function () {
                    scream();
                }, 1000);
            }
        });
}
scream();

app.use('/', router);
app.use(express.static(__dirname + '/public'));

log("Running...");
server.listen(8080, '0.0.0.0');
