var app = require("express");
var server = require("http").Server(app);
var io = require("socket.io")(server);

io.on("connection", function (socket) {
    console.log("Connected: " + socket.id + " with transport " + socket.client.conn.transport.constructor.name);

    var msgCount = 1;

    // to make things interesting, have it send every second
    var interval = setInterval(function () {
        var tweet = {
            user: "nodesource",
            text: "Hello, world!",
            index: msgCount
        };
        msgCount += 1;

        console.log("Sending" + JSON.stringify(tweet) + "via transport " + socket.client.conn.transport.constructor.name);
        socket.emit("tweet", tweet);
    }, 1000);

    socket.on("disconnect", function () {
        console.log("Disconnected.");

        clearInterval(interval);
    });

    socket.on('foo', function () {
        socket.emit("bar", {});
        console.log("sent 'bar'");
    });
});

console.log("Running...");

server.listen(8080, '0.0.0.0');
