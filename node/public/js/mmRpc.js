/**
 LICENSE INFORMATION: "Simplified BSD License" or "FreeBSD License"
 ------------------------------------------------------------------

 Copyright (c) 2015, Gregor Fabritius (fabritius@grefab.de)
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.


 ------------------------------------------------------------------

 File History:

 Created by Gregor Fabritius on 23.03.15.

 2015-03-24 v0.9.0 Beta release
 2015-03-25 v0.9.1 Working with callbacks for query responses.
 2015-03-25 v0.9.2 Hybrid contentHandler: Either callback sendResponse() or return response.
 2015-03-25 v0.9.3 Got rid of "module is not defined" error in browser.

 **/


var MmRpc = {
    /**
     * Creates a UUID-like string that we use as message id. See http://stackoverflow.com/a/105074/72082 for details.
     *
     * @returns {string}
     */
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },

    /**
     * Sanitizes query options by providing default values if necessary.
     *
     * @param options
     * @returns {*|{}}
     */
    ensureOptions: function (options) {
        options = options || {};
        options.timeout = options.timeout || 5000;
        options.maxRetries = options.maxRetries || -1;
        options.success = options.success || undefined;
        options.problem = options.problem || undefined;
        options.error = options.error || undefined;
        options.complete = options.complete || undefined;

        return options;
    },

    /**
     * Performs a query on a topic with content over a socket. Depending on the options, the query is retried multiple
     * times if not answered properly within a timeout.
     *
     * For each query, an unique message id is created and the socket listens temporarily for messages with this topic
     * via socket.on(msgId).
     *
     * A query is successful if a response with this topic is received from the socket endpoint.
     * A query is problematic if it had to be retried.
     * A query has failed when too many retries happened.
     *
     * @param socket       Socket that implements emit() and on() so transmission over network happens.
     * @param topic        Topic registered via on() by socket endpoint.
     * @param content      Content that will be transmitted to endpoint.
     * @param options      Options for transmission including event handlers. See Client.query for details.
     * @param retries      Already attempted retries for this query.
     * @param problemNoted If true, options.problem() event handler will not be called upon next retry.
     * @param queryId      An auto-generated id that is carried over to retries. It is needed to cancel pending queries.
     *
     * @returns Function that, when called, cancels this query.
     */
    doQuery: function (socket, topic, content, options, retries, problemNoted, queryId) {
        var msgId = MmRpc.guid();
        options = MmRpc.ensureOptions(options);
        queryId = queryId || MmRpc.guid();

        /* Internal handler to react when we encounter a timeout */
        var timeoutInterval = setInterval(function () {
            cancelInternalEventHandlers();
            if (retries < options.maxRetries || options.maxRetries < 0) {
                if (options.problem && !problemNoted) {
                    options.problem();
                }
                MmRpc.doQuery(socket, topic, content, options, retries + 1, true, queryId);
            } else {
                if (options.error) {
                    options.error();
                }
                if (options.complete) {
                    options.complete();
                }
            }
        }, options.timeout);

        /* Internal handler to react when we receive a reply to our message */
        var responseHandler = function (content) {
            cancelInternalEventHandlers();

            if (options.success) {
                options.success(content);
            }
            if (options.complete) {
                options.complete();
            }
        };
        socket.on(msgId, responseHandler);

        /* Removes internal event handlers, so they are not accidentally called multiple times. */
        function cancelInternalEventHandlers() {
            delete MmRpc.queryCanceller[queryId];

            clearInterval(timeoutInterval);
            socket.removeListener(msgId, responseHandler);
        }

        MmRpc.queryCanceller[queryId] = cancelInternalEventHandlers;

        /* Send the content via socket. Note that we create a message id for response reference. */
        socket.emit(topic, {
            msgId: msgId,
            content: content
        });

        return function () {
            MmRpc.cancelQuery(queryId);
        }
    },

    /**
     * Storage for pending queries (keys) and their cancellation functions (values).
     */
    queryCanceller: {},

    cancelQuery: function (queryId) {
        if (MmRpc.queryCanceller.hasOwnProperty(queryId)) {
            MmRpc.queryCanceller[queryId]();
        }
    },

    /**
     * Receives a query and a contentHandler to handle the response. When response is figured out by contentHandler,
     * EITHER sendResponse(result) MUST be called within contentHandler OR, as an alternative, contentHandler MUST
     * return a response different than undefined (and queryHandler will call sendResponse by itself).
     * Otherwise the request will time out!
     *
     * @param socket
     * @param query
     * @param contentHandler This it a function(content, sendResponse(response)). Job of contentHandler ist to react on
     *                       the request depending on its content.
     */
    queryHandler: function (socket, query, contentHandler) {
        var msgId = query.msgId;
        var content = query.content;

        var sendResponse = function (response) {
            socket.emit(msgId, response);
        };

        var response = contentHandler(content, sendResponse);
        if (response !== undefined) {
            sendResponse(response);
        }
    },

    Client: function (clientId, endpoint, options) {
        var socket = io.connect(endpoint, options);

        /* Announce ourselves (i.e. the client's id) to the server. Client id is needed to be called by Server.tell().
         */
        function announceId() {
            MmRpc.doQuery(socket,
                "_setId",
                {
                    clientId: clientId
                },
                {
                    timeout: 1000,
                    maxRetries: -1
                },
                0,
                false,
                undefined
            );
        }

        announceId();

        /* Re-announce client's id on reconnect. Reconnection changes socket and therefore the server needs to know.
         */
        socket.on('reconnect', function () {
            announceId();
        });

        /**
         * Sends a query to the server. A query can be answered successfully,
         * it can be in a temporary problematic state, or it can fail.
         * For a query to be answered it is necessary that the server registered the query's
         * topic via Server.on(topic).
         *
         * @param topic   The topic the opposite site is listening to
         * @param content Query content that is sent to the opposite side
         * @param options Object
         *                May contain the following parameters:
         *                  timeout:    Milliseconds before another sending attempt may be made. If a timeout is reached,
         *                              sending is tried again if retries <= maxRetries. Furthermore, problem event
         *                              handler is called, which happens only on the first retry.
         *                  maxRetries: Number of retries that will happen before handling this as an error. If
         *                              set to -1, retrying will never stop.
         *                May contain the following event handlers:
         *                  success:  Called as function(content) when the server answered. Content is the server's
         *                            response.
         *                  problem:  Called as function() when the first retry is attempted.
         *                  error:    Called as function() when maxRetries is reached.
         *                  complete: Called after success or error.
         *
         * @returns Function that, when called, cancels this query.
         */
        this.query = function (topic, content, options) {
            return MmRpc.doQuery(socket, topic, content, options, 0, false, undefined);
        };

        /**
         * Registers a handler that is called when a certain topic is received from the server.
         * The corresponding server call is Server.tell().
         *
         * @param topic   The topic that shall be handled
         * @param handler A handler function(content) { return true|false; }. It receives content as parameter.
         *                If handler returns true, Server.tell() is satisfied and cancels all remaining
         *                connection attempts.
         */
        this.on = function (topic, handler) {
            socket.on(topic, (function (topic, handler) {
                return function (query) {
                    MmRpc.queryHandler(socket, query, handler);
                }
            })(topic, handler));
        };
    },

    Server: function (io, _, logger) {
        logger = logger || function () {
        };

        var handlers = {};

        var socketsByClientId = {};
        var clientIdBySocketId = {};

        /**
         * Registers a handler that is called when a certain topic is requested by the client.
         * The corresponding client call is Client.query().
         *
         * @param topic The topic that shall be handled
         * @param handler A handler function(content) { return "xxx"; }. It receives the request content as parameter.
         *                It may return a response that will be send to the client.
         */
        this.on = function (topic, handler) {
            handlers[topic] = handler;
        };

        /**
         * Sends a message to ALL connections (i.e. connected clients) that belong to clientId. This is successful
         * if ANY of the clients acknowledges the message by responding true. When the first client acknowledges the
         * message, possible pending message sending attempts are cancelled.
         *
         * @param clientId
         * @param topic
         * @param content
         * @param options
         */
        this.notifyClient = function (clientId, topic, content, options) {
            var sockets = socketsByClientId[clientId];
            options = MmRpc.ensureOptions(options);

            if (!sockets) {
                logger("Cannot send to client: Client not known: " + clientId);
                if (options.error) {
                    options.error();
                }
                if (options.complete) {
                    options.complete();
                }
            } else {
                var errorCount = 0;
                var problemCount = 0;

                var cancellers = _.map(sockets, function (socket) {
                    function cancelAllQueries() {
                        _.forEach(cancellers, function (canceller) {
                            canceller();
                        })
                    }

                    return MmRpc.doQuery(
                        socket,
                        topic,
                        content,
                        {
                            timeout: options.timeout,
                            maxRetries: options.maxRetries,
                            success: function (content) {
                                if (content === true) {
                                    cancelAllQueries();

                                    if (options.success) {
                                        options.success();
                                    }
                                    if (options.complete) {
                                        options.complete();
                                    }
                                } else {
                                    /* Even if this query is handled successfully, the client did not respond true,
                                     * so we handle this like an error.
                                     */
                                    errorCount++;

                                    /* If all queries failed, call notifyClient()'s error handler. */
                                    if (errorCount >= cancellers.length) {
                                        cancelAllQueries();

                                        if (options.error) {
                                            options.error();
                                        }
                                        if (options.complete) {
                                            options.complete();
                                        }
                                    }
                                }
                            },
                            problem: function () {
                                problemCount++;

                                /* If all queries are problematic, call notifyClient()'s error handler. */
                                if (problemCount + errorCount >= cancellers.length) {
                                    if (options.problem) {
                                        options.problem();
                                    }
                                }
                            },
                            error: function () {
                                problemCount--; // Errors have been problems before.
                                errorCount++;

                                /* If all queries failed, call notifyClient()'s error handler. */
                                if (errorCount >= cancellers.length) {
                                    cancelAllQueries();

                                    if (options.error) {
                                        options.error();
                                    }
                                    if (options.complete) {
                                        options.complete();
                                    }
                                }
                            }
                        },
                        0,
                        false,
                        undefined);
                });
            }
        };

        /* On every new connection we need to register all handlers we know for that connection.
         * Note that handlers that are newly registered will not be applied to connections that are
         * established before.
         */
        io.on("connection", function (socket) {
            for (var topic in handlers) {
                if (handlers.hasOwnProperty(topic)) {
                    var handler = handlers[topic];

                    socket.on(topic, (function (topic, handler) {
                        return function (query) {
                            MmRpc.queryHandler(socket, query, handler);
                        }
                    })(topic, handler));
                }
            }

            /**
             * Makes sure socketByClientId and clientIdBySocketId are clean of anything concerning the socket given. If
             * this is the last socket connection belonging to a client, the client is removed from the list of connected clients.
             *
             * @param socketId
             */
            function cleanupSocket(socketId) {
                var clientId = clientIdBySocketId[socketId];
                delete clientIdBySocketId[socketId];

                _.remove(socketsByClientId[clientId], function (n) {
                    return n.id === socketId;
                });

                if (_.isEmpty(socketsByClientId[clientId])) {
                    /* We have no open sockets anymore, so remove this client entirely. */
                    delete socketsByClientId[clientId];
                }

                logger("Currently connected devices: " + _.size(clientIdBySocketId) + " for " + _.size(socketsByClientId) + " clients."
                + " (someone left)");

                if (_.isEmpty(clientIdBySocketId) && _.isEmpty(socketsByClientId)) {
                    logger("We are alone now.");
                }
            }

            /**
             * Stores the connection between client and socket. A client can have multiple connections (and therefore
             * sockets). This happens e.g. if a client is connected from multiple devices and/or browser windows.
             *
             * Every connection that belongs to a client may receive messages produced via Server.tell().
             *
             * @param clientId
             * @param socket
             */
            function storeClientSocketConnection(clientId, socket) {
                socketsByClientId[clientId] = socketsByClientId[clientId] || [];
                socketsByClientId[clientId].push(socket);
                socketsByClientId[clientId] = _.uniq(socketsByClientId[clientId], function (socket) {
                    return socket.id;
                });
                clientIdBySocketId[socket.id] = clientId;

                logger("Currently connected devices: " + _.size(clientIdBySocketId) + " for " + _.size(socketsByClientId) + " clients."
                + " (someone joined)");
            }

            /* When a client sends us its id we store it, so that tell() knows whom to speak to.
             */
            socket.on("_setId", function (query) {
                MmRpc.queryHandler(socket, query, function (content) {
                    storeClientSocketConnection(content.clientId, socket);
                    return true;
                });
            });

            /* Clean up this socket from our storage of connected sockets upon disconnect. */
            socket.on("disconnect", function () {
                cleanupSocket(socket.id);
            });
        });
    }
};


/* Node.JS part of this file. */
try {
    module.exports =
    {
        MmRpcServer: MmRpc.Server
    };
} catch (err) {
    /* Do nothing. THis may fail when included as script in HTML. */
}
